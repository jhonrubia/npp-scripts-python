# encoding=utf-8
import os;
import sys;
import Npp
from Npp import *
#filePathSrc="C:\Users\jhonrubia\Desktop\desde\test"
filePathSrc=Npp.notepad.prompt(notepad, 'Ruta de archivos','Introduce la ruta del directorio a procesar')
filePathSrc.replace("\\","\\\\")
filePathSrc = filePathSrc.decode('utf8')
codi=Npp.notepad.prompt(notepad, 'Codificacion de destino','ansi,utf8,utf8bom')
#encodin=notepad.prompt(notepad, '')
encodingMap = { BUFFERENCODING.COOKIE : 'UTF-8 without BOM', BUFFERENCODING.ENC8BIT : 'ANSI', BUFFERENCODING.UTF8 : 'UTF-8' }
mapaCod= {BUFFERENCODING.UTF8 : 'UTF-8', BUFFERENCODING.ENC8BIT : 'ANSI'}
def utf8():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm' or fn[-4:] =='.txt':     
				notepad.open(root + "\\" + fn)

				console.write(root + "\\" + fn + "\r\n")
				console.write("\r\nfileName: " +fn +" :: encoding: " + encodingMap.get(notepad.getEncoding())+ "\r\n")
				console.write(str(notepad.getEncoding()))
				notepad.menuCommand(MENUCOMMAND.FORMAT_CONV2_AS_UTF_8)
				notepad.save()
				notepad.close()
def utf8bom():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm':     
				notepad.open(root + "\\" + fn)
				console.write(root + "\\" + fn + "\r\n")
				console.write("\r\nfileName: " +fn +" :: encoding: " + encodingMap.get(notepad.getEncoding())+ "\r\n")
				notepad.menuCommand(MENUCOMMAND.FORMAT_CONV2_UTF_8)
				notepad.save()
				notepad.close()
def ansi():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm':     
				notepad.open(root + "\\" + fn)
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand( MENUCOMMAND.FORMAT_CONV2_ANSI)
				notepad.save()
				notepad.close()
opciones={'utf8':utf8,'utf8bom':utf8bom,'ansi':ansi,'UTF8':utf8,'UTF-8 BOM':utf8bom,'ANSI':ansi,'UTF8BOM':utf8bom,'UTF-8':utf8}

try:
	opciones[codi]()
except KeyError:
	notepad.messageBox('La codifiacion tiene que ser utf8, utf8bom o ansi','Error')
