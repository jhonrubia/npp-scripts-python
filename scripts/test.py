# coding=utf-8
import os;
import sys;
from Npp import *
#filePathSrc="P:\\Inditex\\070414\\18_DESDE_POSTPRODUCCION\\20170203\\gbcdoc1\\P_Alta\\en_gb"
filePathSrc=notepad.prompt(notepad, 'Ruta de archivos')
filePathSrc.replace("\\","\\\\")
encodin=notepad.prompt(notepad, 'Codificacion de destino. \nOpciones\n1.- UTF8\n2.- UTF8 BOM\n3.- ANSI')
def utf8():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm' or fn[-4:] =='.txt':     
				notepad.open(root + "\\" + fn)
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand(MENUCOMMAND.FORMAT_CONV2_AS_UTF_8)
				notepad.save()
				notepad.close()
def utf8bom():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm':     
				notepad.open(root + "\\" + fn)
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand(MENUCOMMAND.FORMAT_CONV2_UTF_8)
				notepad.save()
				notepad.close()
def ansi():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm':     
				notepad.open(root + "\\" + fn)
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand( MENUCOMMAND.FORMAT_CONV2_ANSI)
				notepad.save()
				notepad.close()
opciones={'1':utf8,'2':utf8bom,'3':ansi}
opciones[encodin]()
