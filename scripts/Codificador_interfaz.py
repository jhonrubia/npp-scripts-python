# encoding=utf-8
import os
import sys
import Npp
from Npp import *
import Tkinter
from Tkinter import *
#from ttk import *
import tkFileDialog
from os import *
import tkMessageBox

#Objeto directorio, solo tiene como atributo la ruta

#Método que recoge la ruta del directorio a procesar
def buscadirectorio():
	direc = tkFileDialog.askdirectory()
	textruta.set(direc)
	#console.write(textruta.get())
	#rutadir = "Ruta: " + str(carpeta)
	#directorio.ruta=textruta.get()

	


#Utiliza el valor del checkbox de codificaciones y lo convierte en el valor de codificación correspondiente en una etiqueta
def selcodificacion():
	codificaciones = {1:'UTF-8',2:'UTF-8 con BOM',3:'ANSI'}
	selection = "Codificación: " + codificaciones[var.get()]
	label.config(text = selection)
#Comprueba que las extensiones de los archivos sean validas
def compruebaExtension(ruta):
	extenOk = True
	listExtensiones=['.htm','.html','.txt','.properties','.xml']
	extensionArchivo = splitext_(ruta)
	#console.write(ruta+"\r\n"+str(extensionArchivo)+"\r\nOk\r\n")
	if not extensionArchivo[1] in listExtensiones:
		extenOk = False
	return extenOk
#Extrae la extension del archivo
def splitext_(path):
	#console.write(path+"\r\n"+ str(splitext(path)))
	if len(path.split('.')) > 2:
		return path.split('.')[0],'.'.join(path.split('.')[-2:])
	return splitext(path)


'''Utiliza el valor del checkbox de codificaciones, lo convierte en la codificación correspondiente, selecciona en comando de NPP correspondiente al cambio de codificación
Recorre el árbol entero de la ruta solicitada, recorre los archivos (html, txt, vm, xml, properties), ejecuta el comando de cambio de codificación, guarda el archivo y cierra el archivo'''
def codificar():
	counterArchivos=0
	codificaciones = {1:'UTF-8',2:'UTF-8 con BOM',3:'ANSI'}
	comandos = {'UTF-8':MENUCOMMAND.FORMAT_CONV2_AS_UTF_8, 'UTF-8 con BOM': MENUCOMMAND.FORMAT_CONV2_UTF_8, 'ANSI':MENUCOMMAND.FORMAT_CONV2_ANSI }
	filePathSrc=textruta.get()
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if compruebaExtension(fn.encode('utf8')):
				#console.write("Ok")
				arc=root+"\\"+fn
				notepad.open(arc.encode('utf8'))
				#console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand(comandos[codificaciones[var.get()]])
				notepad.save()
				notepad.close()
				counterArchivos=counterArchivos+1
			else:
				continue
	if counterArchivos > 0:
		tkMessageBox.showinfo("Completado!", "Se han codificado " +str(counterArchivos)+" archivos")
		
	else:
		tkMessageBox.showerror('Error', 'No se han procesado archivos. Revise las extensiones')


#Genera la ventana de la aplicación
root = Tk()
root.title("Codificador Masivo")
root.geometry('250x250')
#Genera el frame principal
mainframe = Frame(root, bd=5)
mainframe.pack(expand=True)
#Genera el frame del selector de rutas y los objetos que contiene
frameruta = Frame(mainframe)
frameruta.pack(expand=True, anchor=N)
rutalabel = Label(frameruta)
rutalabel.grid(column=1, row=1)
rutalabel.config(text="Ruta:")
textruta = StringVar()
rutaentry = Entry(frameruta, textvariable=textruta)
rutaentry.grid(column=2, row=1)
Button(frameruta, text="Examinar...", command=buscadirectorio,relief="groove").grid(column=3, row=1)

var = IntVar()

codif=""

#Frame para los checkbox del selector de codificación
framecodi= Frame(mainframe,relief="groove", bd=5)
framecodi.pack(anchor=CENTER)
R1 = Radiobutton(framecodi, text="UTF-8 sin BOM", variable=var, value=1,
                  command=selcodificacion)
R1.pack( anchor = CENTER )
R2 = Radiobutton(framecodi, text="UTF-8 con BOM", variable=var, value=2,
                  command=selcodificacion)
R2.pack( anchor = CENTER )
R3 = Radiobutton(framecodi, text="ANSI", variable=var, value=3,
                  command=selcodificacion)
R3.pack( anchor = CENTER)
label = Label(framecodi)
label.pack()

#Frame para el botón "Convertir"
frameconvert = Frame(mainframe,relief="groove", bd=5)
frameconvert.pack(anchor=S)
Button(frameconvert, text='Convertir', command=codificar).pack()

#Frame barra de progreso
'''frameprogress = Frame(mainframe, relief="groove", bd=1)
frameprogress.pack(anchor=S)
progreso = ttk.Progressbar(frameprogress,orient='horizontal',mode='determinate',maximum=100)'''

root.mainloop()