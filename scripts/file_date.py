import calendar
from datetime import datetime
import os
import glob, os

mypath = "P:\\SEPE\\130348\\19_PARA_FINAL"
timefmt = "%Y%m%d %H:%M:%S"
start = calendar.timegm(datetime.strptime("20170105 00:00:00", timefmt).timetuple())
end = calendar.timegm(datetime.strptime("20170208 00:00:00", timefmt).timetuple())

def test(f):
    if (not os.path.isfile(f)):
        return 0
    (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(f)

    return start<=ctime and end>=ctime

files = [f for f in glob.glob(os.path.join(mypath, "*")) if test(f)]
for f in files:
   print(f)