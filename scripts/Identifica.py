import os;
import sys;
import re;
import chardet;
### User Defined Variables ###
filePathSrc=Npp.notepad.prompt(notepad, 'Ruta de archivos')
logFile = open("C:\\EncodingFix.log", "w")
foundCount = 1
encodingMap = { BUFFERENCODING.COOKIE : 'UTF-8 without BOM', BUFFERENCODING.ENC8BIT : 'ANSI', BUFFERENCODING.UTF8: 'UTF-8' }
textToWrite = "Starting Script...\n"
console.write(textToWrite)
logFile.write(textToWrite)
for root, subFolders, files in os.walk(filePathSrc): # searches file path recursively
    textToWrite = "Scanning: " +root + "\n"
    console.write(textToWrite)
    logFile.write(textToWrite)
    for file in files:
        filePath = os.path.join(root,file)
        # only do this for html files
        if file[-4:].lower() == '.htm' or file[-5:].lower() == '.html':
            notepad.open(filePath.decode(sys.getfilesystemencoding()).encode('utf8'))           
            # BUFFERENCODING.COOKIE is returned for files that are "UTF-8 without BOM" or no Encoding menu option selected
            if (notepad.getEncoding() == BUFFERENCODING.COOKIE):
                # use the Universal Encoding Detector (http://chardet.feedparser.org)
                rawdata=open(filePath,"r").read()
                UED_Result = chardet.detect(rawdata)
                UED_Result_Encoding = UED_Result.get("encoding")
                UED_Result_Confidence = UED_Result.get("confidence")
                if UED_Result_Encoding.startswith("ISO-8859") or UED_Result_Encoding.startswith("ascii"):
                    textToWrite = "%d: %s %f %s" % (foundCount, "Chardet Detection   -> " +filePath +": " + UED_Result_Encoding + " [ Confidence:",
                    UED_Result_Confidence, "]\n")
                    console.write(textToWrite)
                    logFile.write(textToWrite)
                    notepad.runMenuCommand("Encoding", "Encode in ANSI") #IMPORTANT: preserve certain chars (Notepad++ seems to hide them)
                else: # Notepad++ detected this file as UTF-8 without a BOM
                    textToWrite = "%d: %s" % (foundCount, "Notepad++ Detection -> " +filePath +": " +encodingMap.get(notepad.getEncoding(),'UNKNOWN') + "\n")
                    console.write(textToWrite)
                    logFile.write(textToWrite)                      
                notepad.runMenuCommand("Encoding", "Convert to UTF-8")                  
                notepad.save()
                foundCount += 1             
            notepad.close()
textToWrite = "Program Completed successfully!\n"
console.write(textToWrite)
logFile.write(textToWrite)
logFile.close