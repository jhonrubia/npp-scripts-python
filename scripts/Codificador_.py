# encoding=utf-8
import os;
import sys;
from Npp import *
#reload(sys)
filePathSrc=notepad.prompt(notepad, 'Ruta de archivos')
filePathSrc.replace("\\","\\\\")
encodin=notepad.prompt(notepad, 'Codificacion de destino.  Opciones 1.- UTF8 2.- UTF8 BOM 3.- ANSI')
def utf8():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm' or fn[-4:] =='.txt' or fn[-4] == '.xml':
				arc=root+"\\"+fn
				notepad.open(arc.decode(sys.getfilesystemencoding()).encode('utf8'))
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand(MENUCOMMAND.FORMAT_CONV2_AS_UTF_8)
				notepad.save()
				notepad.close()
def utf8bom():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm' or fn[-4:] =='.txt' or fn[-4] == '.xml':
				arc=root+"\\"+fn
				notepad.open(arc.decode(sys.getfilesystemencoding()).encode('utf8'))
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand(MENUCOMMAND.FORMAT_CONV2_UTF_8)
				notepad.save()
				notepad.close()
def ansi():
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm' or fn[-4:] =='.txt' or fn[-4] == '.xml':
				arc=root+"\\"+fn
				notepad.open(arc.decode(sys.getfilesystemencoding()).encode('utf8'))
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand( MENUCOMMAND.FORMAT_CONV2_ANSI)
				notepad.save()
				notepad.close()
opciones={'1':utf8,'2':utf8bom,'3':ansi}
opciones[encodin]()
