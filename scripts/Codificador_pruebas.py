# encoding=utf-8
import os
import sys
import Npp
from Npp import *
import Tkinter
from Tkinter import *
import tkFileDialog

class directorio:
	ruta =""

def buscadirectorio():
	direc = tkFileDialog.askdirectory()
	rutadir = "Ruta: " + str(direc)
	directorio.ruta=direc
	# print rutadir
	rutalabel.config(text = rutadir)

def selcodificacion():
	codificaciones = {1:'utf8',2:'utf8bom',3:'ansi'}
	selection = "Codificación: " + codificaciones[var.get()]
	label.config(text = selection)

def codificar():
	codificaciones = {1:'utf8',2:'utf8bom',3:'ansi'}
	comandos = {'utf8':MENUCOMMAND.FORMAT_CONV2_AS_UTF_8, 'utf8bom': MENUCOMMAND.FORMAT_CONV2_UTF_8, 'ansi':MENUCOMMAND.FORMAT_CONV2_ANSI }
	filePathSrc=directorio.ruta
	for root, dirs, files in os.walk(filePathSrc):
		for fn in files:
			if fn[-4:] == '.htm' or fn[-5:] == '.html' or fn[-3:] == '.vm' or fn[-4:] =='.txt' or fn[-4:] == '.xml':
				arc=root+"\\"+fn
				notepad.open(arc.encode('utf8'))
				console.write(root + "\\" + fn + "\r\n")
				notepad.menuCommand(comandos[codificaciones[var.get()]])
				notepad.save()
				notepad.close()
	


root = Tk()
root.title("Codificador Masivo")

mainframe = Frame(root, relief=SUNKEN)
mainframe.pack(expand = True)
frameruta = Frame(mainframe)
frameruta.pack(expand=True)




Button(frameruta, text="Elegir carpeta", command=buscadirectorio).pack(anchor = CENTER)
rutalabel = Label(frameruta)
rutalabel.pack()

var = IntVar()

codif=""

R1 = Radiobutton(root, text="UTF-8 sin BOM", variable=var, value=1,
                  command=selcodificacion)
R1.pack( anchor = CENTER )

R2 = Radiobutton(root, text="UTF-8 con BOM", variable=var, value=2,
                  command=selcodificacion)
R2.pack( anchor = CENTER )

R3 = Radiobutton(root, text="ANSI", variable=var, value=3,
                  command=selcodificacion)
R3.pack( anchor = CENTER)

label = Label(root)
label.pack()

Button(root, text='Convertir', command=codificar).pack()


root.mainloop()